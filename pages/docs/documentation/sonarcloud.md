---
 hide:
#   - footer
---

SonarCloud er et analyse værktøj, som hjælper med at identificere sårbarheder i ens kode. Dette projekt fremviser et eksempel på udnyttelsen af dette værktøj.
SonarCloud sættes op som en del af ens SecDevOps pipeline, således at deploy kan fejle, hvis SonarCloud identificerer en overtrædelse af ens regelopsætning. Default regelopsætningen kan ses her:

![quality_gate](../images/quality_gate.png)

Opsætningen for workflow, som bliver eksekveret af master-workflow, ses her:

```
name: SonarCloud
on:
  workflow_call:
  
jobs:
  build:
    name: Build and analyze
    runs-on: windows-latest
    steps:
      - name: Set up JDK 17
        uses: actions/setup-java@v3
        with:
          java-version: 17
          distribution: 'zulu' # Alternative distribution options are available.
      - uses: actions/checkout@v3
        with:
          fetch-depth: 0  # Shallow clones should be disabled for a better relevancy of analysis
      - name: Cache SonarCloud packages
        uses: actions/cache@v3
        with:
          path: ~\sonar\cache
          key: ${{ runner.os }}-sonar
          restore-keys: ${{ runner.os }}-sonar
      - name: Cache SonarCloud scanner
        id: cache-sonar-scanner
        uses: actions/cache@v3
        with:
          path: .\.sonar\scanner
          key: ${{ runner.os }}-sonar-scanner
          restore-keys: ${{ runner.os }}-sonar-scanner
      - name: Install SonarCloud scanner
        if: steps.cache-sonar-scanner.outputs.cache-hit != 'true'
        shell: powershell
        run: |
          New-Item -Path .\.sonar\scanner -ItemType Directory
          dotnet tool update dotnet-sonarscanner --tool-path .\.sonar\scanner
      - name: Build and analyze
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}  # Needed to get PR information, if any
          SONAR_TOKEN: ${{ secrets.SONAR_TOKEN }}
        shell: powershell
        run: |
          .\.sonar\scanner\dotnet-sonarscanner begin /k:"Nilsson1_Selvvalgt_Solution" /o:"nilsson1" /d:sonar.token="${{ secrets.SONAR_TOKEN }}" /d:sonar.host.url="https://sonarcloud.io"
          dotnet build
          .\.sonar\scanner\dotnet-sonarscanner end /d:sonar.token="${{ secrets.SONAR_TOKEN }}"
```

Ved hjælp af [SonarClouds GitHub Guide](https://docs.sonarsource.com/sonarcloud/getting-started/github/), kan der nu logges ind på SonarClouds hjemmeside, og man kan tilkoble sit GitHub repository til ens SonarCloud. Dette gøres ved brug af en secret, som sættes ind i ens GitHub repository. Analyseværktøjet vil nu køre, når det bliver kaldt fra GitHub Workflow.

![secrets](../images/secrets.png)

SonarCloud kan nu vise analysen af ens repository.

![sonarcloud_forside](../images/quality_gate_passed.png)

Det ses, at "Quality Passed", hvilket betyder, at vi "får lov" til at deploye vores nyeste push.

Under "Issues" vises de forbedringer, som SonarCloud forslår. 

![sonarcloud_issues](../images/sonarcloud_issues.png)

Det ses, at der er en Maintainability Issue som er klassifiseret som "Severe"

Den kan åbnes, og SonarCloud kommer med en kort beskrivelse af problemet og løsningsforslag.

![sonarcloud_severe](../images/severe_description.png)

Efter implementeringen af løsningen, kan det ses, at der ikke længere eksisterer "Severe Issues".

![severe_fixed](../images/severe_fixed.png)

Hvis man filtrerer "Fixed" issues til, kan man se, at SonarCloud har konkluderet den som værende løst.

![conn_fixed](../images/connstring_fixed.png)


### Links
[SonarCloud - GitHub/Getting Started](https://docs.sonarsource.com/sonarcloud/getting-started/github/)