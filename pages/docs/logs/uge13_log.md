---
 hide:
#  - footer
---

# Læringslog uge 13

## Emner
- SonarCloud

## Mål for ugen
### Praktiske mål
- Introduktion til SonarCloud

### Læringsmål

- Viden og forståelse af SonarCloud

## Reflektioner over næste gang
- Opsætning af SonarCloud
- DAST?