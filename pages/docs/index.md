---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Selvvalgt Projekt - Frederik Østerdal Nilsson

## Projektbeskrivelse
Dette projekt omhandler SecDevOps for en WebApplication og hvordan denne udviklingsprocess opsættes, anvendes og testes.

Link til: [SecDevOps WebApp GitHub](https://github.com/Nilsson1/Selvvalgt_Solution)

Link til: [SecDevOps WebApp Website](https://selvvalgtprojekt.azurewebsites.net/)

## Læringsmål

**Viden**

-	Forståelse for SecDevOps som princip
-	Viden om Microsoft Azure
-	Viden om SAST
-	Viden om DAST

**Færdigheder**

-	Oprette en pipeline i GitHub workflow
-	Oprette Unit Tests
-	Anvende SAST teknologier
-	Anvende DAST teknologier

**Kompetencer**

-	Håndtere fejl i Workflow
-	Håndtere teknologier, som bruges til SecDevOps
-	Håndtere udvælgelsen af teknologier