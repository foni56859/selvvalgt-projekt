---
 hide:
#  - footer
---

# Læringslog uge xx - *Titel*

## Emner

Ugens emner er:

- ..
- ..

## Mål for ugen

Herunder kan du læse hvilke må vi har arbejdet med i denne uge

### Praktiske mål

**Praktiske opgaver vi har udført**


- ..
- ..

### Læringsmål

**Læringsmål vi har arbejdet med**

- **Viden:** ..
- **Færdigheder:** ..
- **Kompetencer:** ..


## Reflektioner over hvad vi har lært

- ..

## Andet