---
 hide:
#  - footer
---

# Læringslog uge 10

## Emner
- SAST
- DAST
- CodeQL
- GitHub Workflows
- Snyk

## Mål for ugen
- Introduktion til SAST
- Introduktion til DAST
- Flere Workflow opsætning
- CodeQL

### Praktiske mål
- Opsættelse af CodeQL SAST værktøj til repository

### Læringsmål
- Viden, forståelse og håndtering af SAST -> CodeQL
- Viden, forståelse og håndtering af workflows/yml

## Reflektioner over næste gang
- Opsættelse af DAST værktøjer
- Snyk?
- Opdel i flere yml filer?
    - Analyse
        - CodeQL
        - Snyk?
    - Build
        - Unit Test?
        - DAST?
    - Deploy