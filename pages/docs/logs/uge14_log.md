---
 hide:
#  - footer
---

# Læringslog uge 14

## Emner
- SonarCloud

## Mål for ugen
### Praktiske mål
- Opsætning af SonarCloud
- Konvertering af automatisk skanning til integreret i github workflow

### Læringsmål

- Viden, forståelse og håndtering af SonarCloud
- Viden, forståelse og håndtering af workflows/yml
- Opsætning af SonarCloud

## Reflektioner over næste gang
- Sekvensiel eksekvering af SonarCloud, således der ikke kan deployes, hvis checked ikke pass'er.
- DAST?