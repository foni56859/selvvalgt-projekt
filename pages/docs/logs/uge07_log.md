---
 hide:
#  - footer
---

# Læringslog uge 07 - Introduktion til Selvvalgt Projekt

## Emner

## Mål for ugen

### Praktiske mål
- Undersøg emner...

### Læringsmål

*I denne uge arbejdede vi ikke med konkrette læringsmål fra studieordningen, men forståelsen for dem*

## Reflektioner over næste gang

- Hvordan læringsmålene skal forstås
- Semesterstrukturen