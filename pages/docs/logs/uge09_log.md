---
 hide:
#  - footer
---

# Læringslog uge 09 - Azure/In-memory DB

## Emner
- Microsoft Azure
- In-memory Databaser

## Mål for ugen

### Praktiske mål
- Læs Microsoft Azure WebApp setup guide
- In-memory databaser .NET dokumentation

### Læringsmål

**Viden**
- Forståelse for Azure
- Forståelse for In-memory Databaser

**Færdigheder**
- Opsætning af Webapp i Azure

## Reflektioner over næste gang
- SAST
