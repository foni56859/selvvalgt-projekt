---
 hide:
#   - footer
---

GitHub anvendes som udviklingsstyringsmiljø. 

GitHub har sikkerhedsindstillinger, som er vigtige at slå til, hvis man ønsker at forbedre sikkerheden.

![github_sec](../images/security_overview.png)

Derudover kan man slå CodeQL code scan til.

![github_codescan](../images/codescan_github.png)