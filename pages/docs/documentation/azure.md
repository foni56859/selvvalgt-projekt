---
 hide:
#   - footer
---

Dette projekt anvender Microsoft Azure, som hosting platform til en WebApplikation, som anvender SecDevOps principper til udviklingen.

Der oprettes en Web App i Azure, ved at følge opsætningen, som Azure præsenterer én for. GitHub repository linkes til Azure Web App'en.

Azure genererer et link til hjemmesiden, som er dannet ud fra GitHub repositoriet:

[https://selvvalgtprojekt.azurewebsites.net/](https://selvvalgtprojekt.azurewebsites.net/)

WebApp projektet kan nu ses i dashboarded.

![azure_forside](../images/azure_forside.png)

LogStream sættes til.

![logstream](../images/log_stream.png)

Under Deployment Center kan man se alle push, som er blevet deployed.

![azure_deployment_center](../images/deployment_center.png)

Hjemmesiden henter dummy data fra dens In-memory DataBase og viser det på siden.

![website](../images/website.png)

### Links
