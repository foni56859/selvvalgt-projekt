---
 hide:
#  - footer
---

# Læringslog uge 12

## Emner
- CodeQL
- GitHub Workflows

## Mål for ugen
### Praktiske mål
- Workflow kan kalde andre workflows
- Sekvensiel udførsel af Workflows (således analyze og deploy ikke kan køre parallelt...)

### Læringsmål
- Viden, forståelse og håndtering af workflows/yml

## Reflektioner over næste gang
- Konvertering af automatisk skanning til integreret i github workflow