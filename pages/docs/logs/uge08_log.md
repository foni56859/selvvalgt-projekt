---
 hide:
#  - footer
---

# Læringslog uge 08 - Introduktion til SecDevOps

## Emner
-  SecDevOps

## Mål for ugen

### Praktiske mål
- Undersøg SecDevOps
- Læs GitHub Workflow dokumentation

### Læringsmål

**Viden**
- Hvad er SecDevOps
- Hvad er et GitHub WorkFlow

## Reflektioner over næste gang

