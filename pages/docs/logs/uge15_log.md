---
 hide:
#  - footer
---

# Læringslog uge 15

## Emner
- SonarCloud

## Mål for ugen
### Praktiske mål
- Opsætning af SonarCloud
- Sekvensiel eksekvering af SonarCloud, således der ikke kan deployes, hvis checked ikke pass'er.
- Anvend værktøj til at finde en fejl - og fikse den

### Læringsmål

- Viden, forståelse og håndtering af SonarCloud
- Viden, forståelse og håndtering af workflows/yml
- Håndtering af sårbarheder fundet ved SonarCloud

## Reflektioner over næste gang
- DAST?